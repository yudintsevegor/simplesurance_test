package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"sync"
	"time"
)

type deadlines struct {
	Deadlines []int64 `json:"deadlines"`
}

type counter struct {
	fileName   string
	timeWindow time.Duration

	reqTimestampDeadlines []int64
	mu                    sync.Mutex
}

func newCounter(fileName string, timeWindow time.Duration) (*counter, error) {
	// TODO: if fileName or timeWindow are empty - fail

	fileData, err := os.ReadFile(fileName)
	fileExists := !errors.Is(err, os.ErrNotExist)
	if err != nil && fileExists {
		return nil, fmt.Errorf("reading file %s: %v", fileName, err)
	}

	di := deadlines{Deadlines: make([]int64, 0, 1)}
	if fileExists {
		if err := json.Unmarshal(fileData, &di); err != nil {
			return nil, fmt.Errorf("unmarshalling data from file: %v", err)
		}
	}

	cnt := &counter{
		fileName:              fileName,
		timeWindow:            timeWindow,
		reqTimestampDeadlines: di.Deadlines,
		mu:                    sync.Mutex{},
	}

	return cnt, nil
}

type responseAPI struct {
	Count int `json:"count"`
}

func (c *counter) counterHandler(writer http.ResponseWriter, _ *http.Request) {
	log.Println("receive request")

	now := time.Now().UTC()
	c.add(now)

	if err := c.update(now); err != nil {
		log.Printf("updating counter data: %v\n", err)
		writer.WriteHeader(http.StatusInternalServerError)

		return
	}

	if err := c.save(); err != nil {
		log.Printf("saving data to the filesystem: %v\n", err)
		writer.WriteHeader(http.StatusInternalServerError)

		return
	}

	resp := responseAPI{Count: len(c.reqTimestampDeadlines)}
	bytesResp, err := json.Marshal(resp)
	if err != nil {
		log.Printf("marshalling response to the user: %v\n", err)
		writer.WriteHeader(http.StatusInternalServerError)

		return
	}

	_, _ = writer.Write(bytesResp)
}

func (c *counter) add(t time.Time) {
	deadline := t.Add(c.timeWindow).Unix()

	c.mu.Lock()
	defer c.mu.Unlock()

	c.reqTimestampDeadlines = append(c.reqTimestampDeadlines, deadline)
}

func (c *counter) update(t time.Time) error {
	currentTimestamp := t.Unix()
	c.mu.Lock()
	defer c.mu.Unlock()

	updatedReqs := make([]int64, 0, len(c.reqTimestampDeadlines))
	for _, deadline := range c.reqTimestampDeadlines {
		if deadline-currentTimestamp <= 0 {
			continue
		}

		updatedReqs = append(updatedReqs, deadline)
	}
	c.reqTimestampDeadlines = updatedReqs

	return nil
}

func (c *counter) save() error {
	c.mu.Lock()
	defer c.mu.Unlock()

	d := deadlines{Deadlines: c.reqTimestampDeadlines}
	bytes, err := json.Marshal(d)
	if err != nil {
		return fmt.Errorf("marsahlling data for the saving: %v", err)
	}

	if err := os.WriteFile(c.fileName, bytes, 0666); err != nil {
		return fmt.Errorf("writing data to file %s: %v", c.fileName, err)
	}

	return nil
}
