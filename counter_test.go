package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

func Test_newCounter(t *testing.T) {
	t.Run("file with data exists", func(t *testing.T) {
		const (
			testFile     = "./testdata/existing_file.json"
			testDeadline = 47128398127
			timeWindow   = 60 * time.Second
		)

		testdata := []byte(fmt.Sprintf(`{"deadlines":[%d]}`, testDeadline))
		if err := os.WriteFile(testFile, testdata, 0666); err != nil {
			t.Error(err)
		}

		cnt, err := newCounter(testFile, timeWindow)
		if err != nil {
			t.Error(err)
		}

		if len(cnt.reqTimestampDeadlines) != 1 {
			t.Errorf("req deadlines should be equal to 1, but equal to %d", len(cnt.reqTimestampDeadlines))
		}
		if cnt.reqTimestampDeadlines[0] != testDeadline {
			t.Errorf(
				"first req deadline should be equal to testDeadline, but equal to %d",
				cnt.reqTimestampDeadlines[0],
			)
		}
	})

	t.Run("file with data doesnt exist", func(t *testing.T) {
		const (
			testFile   = "./testdata/unknown_file_13912973892.json"
			timeWindow = 60 * time.Second
		)

		cnt, err := newCounter(testFile, timeWindow)
		if err != nil {
			t.Error(err)
		}

		if len(cnt.reqTimestampDeadlines) != 0 {
			t.Errorf("req deadlines should be equal to 0, but equal to %d", len(cnt.reqTimestampDeadlines))
		}
	})

	t.Run("unknown data format", func(t *testing.T) {
		const (
			testFile   = "./testdata/with_bad_data.json"
			timeWindow = 60 * time.Second
		)

		testdata := []byte(`{"deadlines":"721367"`)
		if err := os.WriteFile(testFile, testdata, 0666); err != nil {
			t.Error(err)
		}

		_, err := newCounter(testFile, timeWindow)
		if err == nil {
			t.Error("should be error")
		}
	})
}

func TestCounter_add(t *testing.T) {
	t.Run("add one element", func(t *testing.T) {
		const (
			timeWindow = 5 * time.Second
			fileName   = "./testdata/unknown_file_31982380127.json"
		)

		cnt, err := newCounter(fileName, timeWindow)
		if err != nil {
			t.Error(err)
		}
		now := time.Now().UTC()
		cnt.add(now)

		if len(cnt.reqTimestampDeadlines) != 1 {
			t.Errorf("req deadlines should be equal to 1, but equal to %d", len(cnt.reqTimestampDeadlines))
		}

		if cnt.reqTimestampDeadlines[0] != now.Add(timeWindow).Unix() {
			t.Errorf(
				"req deadlines should be equal to %d, but equal to %d",
				now.Add(timeWindow).Unix(),
				len(cnt.reqTimestampDeadlines),
			)
		}
	})

	t.Run("add several elements", func(t *testing.T) {
		const (
			timeWindow = 5 * time.Second
			fileName   = "./testdata/unknown_file_328910372108.json"
		)

		cnt, err := newCounter(fileName, timeWindow)
		if err != nil {
			t.Error(err)
		}

		now := time.Now().UTC()
		cnt.add(now)
		tomorrow := now.Add(25 * time.Hour)
		cnt.add(tomorrow)

		if len(cnt.reqTimestampDeadlines) != 2 {
			t.Errorf("req deadlines should be equal to 2, but equal to %d", len(cnt.reqTimestampDeadlines))
		}

		if cnt.reqTimestampDeadlines[0] != now.Add(timeWindow).Unix() {
			t.Errorf(
				"req deadlines should be equal to %d, but equal to %d",
				now.Add(timeWindow).Unix(),
				cnt.reqTimestampDeadlines[0],
			)
		}

		if cnt.reqTimestampDeadlines[1] != tomorrow.Add(timeWindow).Unix() {
			t.Errorf(
				"req deadlines should be equal to %d, but equal to %d",
				tomorrow.Add(timeWindow).Unix(),
				cnt.reqTimestampDeadlines[1],
			)
		}
	})
}

func TestCounter_update(t *testing.T) {
	t.Run("remove 0 elements with deadline", func(t *testing.T) {
		const (
			timeWindow = 60 * time.Second
			fileName   = "./testdata/unknown_file_32712389719.json"
		)

		cnt, err := newCounter(fileName, timeWindow)
		if err != nil {
			t.Error(err)
		}

		var (
			firstT  = time.Now().UTC()
			secondT = firstT.Add(5 * time.Second)
			thirdT  = firstT.Add(10 * time.Second)
		)

		times := []time.Time{firstT, secondT, thirdT}
		for _, tm := range times {
			cnt.add(tm)
		}

		if err := cnt.update(firstT.Add(15 * time.Second)); err != nil {
			t.Error(err)
		}

		if len(cnt.reqTimestampDeadlines) != len(times) {
			t.Errorf(
				"req deadlines should be equal to %d, but equal to %d",
				len(times),
				len(cnt.reqTimestampDeadlines))
		}

		for i := 0; i < len(cnt.reqTimestampDeadlines); i++ {
			if cnt.reqTimestampDeadlines[i] != times[i].Add(timeWindow).Unix() {
				t.Errorf(
					"req deadlines should be equal to %d, but equal to %d",
					times[i].Add(timeWindow).Unix(),
					cnt.reqTimestampDeadlines[i],
				)
			}
		}
	})

	t.Run("remove 1 element with deadline", func(t *testing.T) {
		const (
			timeWindow = 5 * time.Second
			fileName   = "./testdata/unknown_file_3913218093.json"
		)

		cnt, err := newCounter(fileName, timeWindow)
		if err != nil {
			t.Error(err)
		}

		var (
			firstT  = time.Now().UTC()
			secondT = firstT.Add(10 * time.Second)
		)

		cnt.add(firstT)
		cnt.add(secondT)

		if err := cnt.update(firstT.Add(5 * time.Second)); err != nil {
			t.Error(err)
		}

		if len(cnt.reqTimestampDeadlines) != 1 {
			t.Errorf("req deadlines should be equal to 1, but equal to %d", len(cnt.reqTimestampDeadlines))
		}

		if cnt.reqTimestampDeadlines[0] != secondT.Add(timeWindow).Unix() {
			t.Errorf(
				"req deadlines should be equal to %d, but equal to %d",
				secondT.Add(timeWindow).Unix(),
				cnt.reqTimestampDeadlines[0],
			)
		}
	})

	t.Run("remove all elements with deadlines", func(t *testing.T) {
		const (
			timeWindow = 5 * time.Second
			fileName   = "./testdata/unknown_file_3892108371.json"
		)

		cnt, err := newCounter(fileName, timeWindow)
		if err != nil {
			t.Error(err)
		}

		var (
			firstT  = time.Now().UTC()
			secondT = firstT.Add(10 * time.Second)
		)

		cnt.add(firstT)
		cnt.add(secondT)

		if err := cnt.update(firstT.Add(20 * time.Second)); err != nil {
			t.Error(err)
		}

		if len(cnt.reqTimestampDeadlines) != 0 {
			t.Errorf("req deadlines should be equal to 0, but equal to %d", len(cnt.reqTimestampDeadlines))
		}
	})
}

func TestCounter_save(t *testing.T) {
	const (
		fileName   = "./testdata/tmp_file.json"
		timeWindow = 5 * time.Second
	)

	cnt, err := newCounter(fileName, timeWindow)
	if err != nil {
		t.Error(err)
	}

	now := time.Now().UTC()
	cnt.add(now)

	if err := cnt.save(); err != nil {
		t.Error(err)
	}

	data, err := os.ReadFile(fileName)
	if err != nil {
		t.Error(err)
	}

	d := deadlines{Deadlines: cnt.reqTimestampDeadlines}
	expectedData, err := json.Marshal(d)
	if err != nil {
		t.Error(err)
	}

	if !bytes.Equal(data, expectedData) {
		t.Error("content are not equal")
	}

	if err := os.Remove(fileName); err != nil {
		t.Error(err)
	}
}

func TestCounter_counterHandler(t *testing.T) {
	t.Run("one time call", func(t *testing.T) {
		const (
			timeWindow = 60 * time.Second
			fileName   = "./testdata/unknown_file_2891398217398761.json"
		)
		t.Cleanup(func() {
			_ = os.Remove(fileName)
		})

		cnt, err := newCounter(fileName, timeWindow)
		if err != nil {
			t.Error(err)
		}

		req := httptest.NewRequest(http.MethodGet, "/", nil)
		w := httptest.NewRecorder()

		cnt.counterHandler(w, req)

		res := w.Result()
		defer res.Body.Close()

		data, err := io.ReadAll(res.Body)
		if err != nil {
			t.Error(err)
		}

		resp := responseAPI{
			Count: 1,
		}

		expectedData, err := json.Marshal(resp)
		if err != nil {
			t.Error(err)
		}

		if !bytes.Equal(data, expectedData) {
			t.Error("unexpected result", string(data))
		}
	})

	t.Run("2 times call", func(t *testing.T) {
		const (
			timeWindow = 60 * time.Second
			fileName   = "./testdata/unknown_file_2832893798123.json"
		)
		t.Cleanup(func() {
			_ = os.Remove(fileName)
		})

		cnt, err := newCounter(fileName, timeWindow)
		if err != nil {
			t.Error(err)
		}

		req := httptest.NewRequest(http.MethodGet, "/", nil)
		w1 := httptest.NewRecorder()
		w2 := httptest.NewRecorder()

		cnt.counterHandler(w1, req)
		cnt.counterHandler(w2, req)

		res := w2.Result()
		defer res.Body.Close()

		data, err := io.ReadAll(res.Body)
		if err != nil {
			t.Error(err)
		}

		resp := responseAPI{
			Count: 2,
		}

		expectedData, err := json.Marshal(resp)
		if err != nil {
			t.Error(err)
		}

		if !bytes.Equal(data, expectedData) {
			t.Error("unexpected result", string(data))
		}
	})

	// TODO: add more tests, no have time currently.
}
