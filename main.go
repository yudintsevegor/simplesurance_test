package main

import (
	"fmt"
	"log"
	"net/http"
	"time"
)

func main() {
	if err := cmd(); err != nil {
		log.Fatalln(err)
	}
}

func cmd() error {
	const (
		fileName   = "count.json"
		timeWindow = 60 * time.Second
	)

	cnt, err := newCounter(fileName, timeWindow)
	if err != nil {
		return fmt.Errorf("making new counter struct: %v", err)
	}
	http.HandleFunc("/", cnt.counterHandler)

	const port = "8888"
	log.Printf("running service on port %s\n", port)

	return http.ListenAndServe(":"+port, nil)
}
